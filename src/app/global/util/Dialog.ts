import {InformType} from './enum/InformType';
import {InformComponent} from '../../shared/inform/inform.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalDismiss} from './enum/ModalDismiss';

export class Dialog {

  private static modalService: NgbModal;

  public static createInstance(modalService: NgbModal) {
    this.modalService = modalService;
  }

  public static show(informType: InformType, title: string, message?: string, modalHeight?: number, modalDismiss?: ModalDismiss) {
    let modalRef;
    if (modalDismiss !== undefined) {
      if (ModalDismiss.No_Backdrop_Click_And_Esc === modalDismiss) {
        modalRef = this.modalService.open(InformComponent, {backdrop: 'static', keyboard: false});
      }
    } else {
      modalRef = this.modalService.open(InformComponent, {backdrop: 'static'});
    }
    if (message) {
      modalRef.componentInstance.message = message;
    }
    if (modalHeight) {
      modalRef.componentInstance.modalHeight = modalHeight;
    }
    modalRef.componentInstance.informType = informType;
    modalRef.componentInstance.title = title;
  }
}

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {LoginComponent} from './user/component/login/login.component';

import {AppRoutingModule} from './app.routes';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoadingComponent} from './shared/loading/loading.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {HomeComponent} from './user/component/exchange/home/home.component';
import {AuthInterceptorService} from "./user/service/auth-interceptor.service";
import {AddExchangeComponent} from './user/component/exchange/add-exchange/add-exchange.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { InformComponent } from './shared/inform/inform.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoadingComponent,
    HomeComponent,
    AddExchangeComponent,
    InformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

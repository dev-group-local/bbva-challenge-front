import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ExchangeTypeService} from "../../../service/exchange-type.service";
import {Constant} from "../../../../global/util/Constant";
import {debounceTime, Subscription} from "rxjs";
import {ExchangeService} from "../../../service/exchange.service";
import {Exchange} from "../../../model/Exchange";

@Component({
  selector: 'app-add-exchange',
  templateUrl: './add-exchange.component.html',
  styleUrls: ['./add-exchange.component.css']
})
export class AddExchangeComponent implements OnInit {

  // @ts-ignore
  public exchangeForm: FormGroup;
  // @ts-ignore
  private observable: Subscription;
  public loading = false;
  public currencies = [
    {code: 'PEN', description: 'SOLES'},
    {code: 'USD', description: 'DÓLARES'}];

  constructor(public activeModal: NgbActiveModal,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private exchangeService: ExchangeService,
              private exchangeTypeService: ExchangeTypeService) {
    this.createForm();
    // @ts-ignore
    this.exchangeForm.controls['originCurrency'].setValue(this.currencies[0].code)
    // @ts-ignore
    this.exchangeForm.controls['targetCurrency'].setValue(this.currencies[1].code)
  }

  ngOnInit(): void {
    this.observable = this.exchangeForm.controls['originAmount'].valueChanges
      .pipe(debounceTime(500))
      .subscribe(data => {
          this.typeExchange();
        }
      );
  }

  private createForm() {
    this.exchangeForm = this.formBuilder.group(
      {
        originAmount: [''],
        originCurrency: [''],
        targetAmount: [''],
        targetCurrency: [''],
      });
  }

  changeIt() {
    this.loading = true;
    const exchange: Exchange = this.exchangeForm.value;
    this.exchangeService.save(exchange).subscribe(value => {
      this.loading = false;
      this.activeModal.close('ok');
    }, error => {
      this.activeModal.close('no-ok');
      this.loading = false;
    })
  }

  typeExchange(): void {
    this.loading = true;
    this.exchangeForm.controls['targetAmount'].setValue('')
    const url = Constant.ROOT_API_V1.concat('/type-exchanges?',
      'originCurrency=', this.exchangeForm.controls['originCurrency'].value,
      '&originAmount=', this.exchangeForm.controls['originAmount'].value,
      '&targetCurrency=', this.exchangeForm.controls['targetCurrency'].value);
    this.exchangeTypeService.get(url).subscribe((value: any) => {
      this.loading = false;
      this.exchangeForm.controls['targetAmount'].setValue(value.targetAmount)
    }, error => {
      this.loading = false;
    })
  }
}

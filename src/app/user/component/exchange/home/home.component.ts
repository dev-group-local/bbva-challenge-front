import {Component, OnInit} from '@angular/core';
import {Search} from "../../../model/Search";
import {HomeService} from "../../../service/home.service";
import {Constant} from "../../../../global/util/Constant";
import {Exchange} from "../../../model/Exchange";
import {AddExchangeComponent} from "../add-exchange/add-exchange.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public search = new Search();
  public exchanges: Exchange[] = [];
  public loading = false;
  public currencies = [
    {code: '00', description: 'TODO'},
    {code: 'PEN', description: 'SOLES'},
    {code: 'USD', description: 'DÓLARES'}];

  constructor(private homeService: HomeService,
              private modalService: NgbModal) {
    this.search.originCurrency = this.currencies[0].code;
    this.search.targetCurrency = this.currencies[0].code;
  }

  ngOnInit(): void {
    this.searchData();
  }

  public searchData() {
    this.loading = true;
    this.search.originCurrency = this.search.originCurrency ? this.search.originCurrency : '';
    this.search.targetCurrency = this.search.targetCurrency ? this.search.targetCurrency : '';
    this.search.createdAt = this.search.createdAt ? this.search.createdAt : '';
    const url = Constant.ROOT_API_V1.concat('/exchanges?',
      'originCurrency=', this.search.originCurrency,
      '&targetCurrency=', this.search.targetCurrency,
      '&createdAt=', this.search.createdAt);
    this.homeService.get(url).subscribe((value: any) => {
      this.loading = false;
      if (value._embedded) {
        this.exchanges = value._embedded.exchanges;
      } else {
        this.exchanges = [];
      }
    }, error => {
      this.loading = false;
    })
  }

  public openAddExchangeModal() {
    const modalRef = this.modalService.open(AddExchangeComponent, {});
    modalRef.result.then((result) => {
      if ('ok' === result) {
        this.searchData();
      }
    }, reason => {
    });
  }
}

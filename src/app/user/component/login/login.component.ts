import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../model/User";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {InformComponent} from "../../../shared/inform/inform.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {InformType} from "../../../global/util/enum/InformType";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user = new User();
  // @ts-ignore
  public userForm: FormGroup;
  private returnUrl: string | undefined;
  public loading: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private modalService: NgbModal) {
    this.createForm();
  }

  ngOnInit() {
    this.returnUrl = this.router.paramsInheritanceStrategy.anchor('returnUrl' || '/');
  }

  login(user: User) {
    if (this.userForm.invalid) {
      Object.values(this.userForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }
    this.loading = true;
    this.authService.login(user).subscribe(data => {
      this.loading = false;
      this.router.navigate(['home']);
    }, error => {
      this.loading = false;
      const modalRef = this.modalService.open(InformComponent, {backdrop: 'static', keyboard: false});
      modalRef.componentInstance.informType = InformType.Danger;
      modalRef.componentInstance.title = 'Inicio de sesión';
      modalRef.componentInstance.message = 'Sus credenciales son incorrectas, vuelva a intentarlo';

    });
  }

  private createForm() {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public isInvalidControl(ctrl: string): boolean {
    // @ts-ignore
    return this.userForm.get(ctrl).invalid && this.userForm.get(ctrl).touched;
  }
}

export class Exchange {
  originCurrency?: string;
  originAmount?: string;
  targetCurrency?: string;
  targetAmount?: string;
  createdAt?: string;
}

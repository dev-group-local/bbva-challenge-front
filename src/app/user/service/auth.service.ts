import {Injectable} from '@angular/core';
import {Constant} from "../../global/util/Constant";
import {Observable} from "rxjs";
import {User} from "../model/User";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private http: HttpClient) {
  }

  public login(user: User): Observable<any> {
    return this.http.post<any>(Constant.ROOT + '/login', JSON.stringify(user), {observe: 'response'})
      .pipe(map(response => {
        if (response && response.headers) {
          const authorization = response.headers.get('Authorization');
          const token = authorization ? authorization.replace('Bearer ', '') : '';
          localStorage.setItem('token', token);
        }
        return response;
      }));
  }
}

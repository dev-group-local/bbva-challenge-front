import {Injectable} from '@angular/core';
import {Exchange} from "../model/Exchange";
import {Observable} from "rxjs";
import {Constant} from "../../global/util/Constant";
import {HttpClient} from "@angular/common/http";
import {Headers} from "../../global/util/Headers";

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  private url = Constant.ROOT_API_V1.concat('/exchanges');

  constructor(private http: HttpClient) {
  }

  public save(exchange: Exchange): Observable<any> {
    return this.http.post<any>(this.url, JSON.stringify(exchange), Headers.contentType);
  }
}
